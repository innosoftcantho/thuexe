<?php

use App\Http\Middleware\Activated;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/', 'HomeController@index')->name('/');

// Route::view('login','web.login')->name('login');
// Route::view('register','web.register')->name('register');
// Route::view('contact','web.contact')->name('contact');
// Route::view('about','web.about')->name('about');
// Route::view('product-category','web.product-category')->name('productcategory');
// Route::view('category','web.category')->name('category');
// Route::view('content','web.category.content')->name('content');
// Route::view('product','web.product.product')->name('product');
// Route::view('productct','web.product.productct')->name('productct');
// Route::view('product-x','web.product.productx')->name('product-x');
// Route::view('payment','web.payment')->name('payment');
// Route::view('search','web.search')->name('search');

// Auth::routes(['register' => false, 'verify' => true]);

// Route::middleware(['auth', Activated::class])->group(function () {
//     Route::prefix('admin')->group(function () {
//         Route::namespace('Admin')->group(function () {
//             Route::middleware(Admin::class)->group(function () {
//             });
            
//             Route::get('/', 'HomeController@index')->name('admin');
//             Route::get('profile', 'UserController@profile')->name('profile');
    
//             Route::resource('categories', 'CategoryController')->except(['show']);
//             Route::resource('contacts', 'ContactController')->except(['create', 'edit', 'update']);
//             Route::resource('contents', 'ContentController');
//             Route::resource('languages', 'LanguageController')->except(['show']);
//             Route::resource('menus', 'MenuController');
//             Route::resource('sponsors', 'SponsorController')->except('show');
//             Route::resource('users', 'UserController')->except(['show']);
//         });
//     });
// });

// Route::group(['middleware' => ['web']], function () {
//     Route::get('/', 'HomeController@index')->name('home');
//     Route::post('admin', 'Admin\ContactController@store')->name('web.contacts.store');
//     Route::get('{menu}', 'HomeController@menu')->where('menu', '([A-Za-z0-9\-\/]+)');
// });