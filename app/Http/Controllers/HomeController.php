<?php

namespace App\Http\Controllers;

// use App\Store;
// use Auth;
use App;
use App\Menu;
use App\Sponsor;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->menu = new Menu;
        // $this->category = new Category;
        // $this->sponsor = new Sponsor;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $menu = $this->menu->where('alias', '/')->where('lang', app()->getLocale())->firstOrFail();
        // $sections = $menu->categories()->orderBy('sort', 'asc')->get();
        // $category = $menu->categories()->firstOrFail();
        // $contents = $category->contents()->where('contents.is_show', 1)->orderBy('contents.id', 'desc')->take(2)->get();
        // dd($menu->categories()->orderBy('sort')->get());
        return view("web.home",
        [
            // 'categories'    => $this->category->get(),
            // 'sections'      => $sections,
            // 'contents_1'    => $sections[0]->contents()->exists() ? $sections[0]->contents()->take(7)->get() : null,
            // 'contents_2'    => $sections[1]->contents()->exists() ? $sections[1]->contents()->take(4)->get() : null,
            // 'contents_3'    => $sections[2]->contents()->exists() ? $sections[2]->contents()->take(5)->get() : null,
            // 'carousels'     => $sections[3]->contents()->exists() ? $sections[3]->contents : null,
            // 'banners'       => $sections[4]->contents()->exists() ? $sections[4]->contents()->orderBy('created_at', 'desc')->take(4)->get() : null,
            // 'menu'          => $menu,
            // 'menus'         => $this->menu->getMenus(),
            // 'sponsors'      => $this->sponsor->where('is_show', 1)->orderBy('sort')->get(),
            // 'category'      => $category,
            // 'contents'      => $contents,
            // 'menu_category' => $category->menus()->where('alias', '<>', '/')->first(),
        ]);
    }

    public function menu($alias)
    {
        $menus = explode('/', $alias);
        $countMenu = count($menus);
        if ($countMenu > 2) abort(404);
        $menu = $this->menu->where('alias', $menus[0])->where('lang', app()->getLocale())->firstOrFail();
        if ($countMenu == 2)
            return App::make("App\Http\Controllers\Web\\" . Str::studly($menu->type) . "Controller")->detail($menu, $menus[1]);
        return App::make("App\Http\Controllers\Web\\" . Str::studly($menu->type) . "Controller")->index($menu);
    }
}
