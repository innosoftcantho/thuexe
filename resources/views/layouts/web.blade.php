<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('/favicon.ico') }}">
        <title>{{ config('app.name','INNOSOFT') }}</title>
        {{-- Style --}}
        <link rel="stylesheet" href="{{ asset('css/web.css') }}?t={{ now() }}">
        @stack('css')
    </head>
    <body>
        <div id="web">
            @include('shared.navbar')

            @yield('content')

            @include('shared.footer')
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/web.js') }}"></script>
        @stack('js')

        <script>
            $(document).ready(function(){
                @stack('ready')
                $(window).on("scroll", function() {
                    if (screen.width >= 1200) {
                        if (document.documentElement && !document.body.scrollTop) {
                            if (document.documentElement.scrollTop <= 35) {
                                $(".header2").removeClass("sticky-top");
                            }
                            else if (!$(".header2").hasClass("sticky-top")) {
                                $(".header2").addClass("sticky-top");
                            }
                        }
                        else {
                            if (document.body.scrollTop <= 80) {
                                $(".header2").removeClass("sticky-top");
                            }
                            else if (!this.el.classList.contains(this.classToggle)) {
                                $(".header2").addClass("sticky-top");
                            }
                        }
                    }
                    else {
                        $(".header2").addClass("sticky-top");
                    }
                });
            });
        </script>
    </body>
</html>