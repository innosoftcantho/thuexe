<div class="bg-center bg-cover bg-repeat" style="background-image: url( {{ asset('img/header.png') }} )">
    <div class="search py-lg-5">
        <div class="container h-100 py-4 py-lg-5">
            <div class="row d-flex align-items-center h-100">
                <div class="col-lg-8 pb-4 pb-lg-0">
                    <div class="text-white text-uppercase font-weight-bold text-header">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="rounded bg-white p-3">
                        <div class="box-title font-weight-bold d-flex justify-content-center mb-3">Tìm xe chỉ với 4 bước</div>
                        <label for="">Ngày nhận xe</label>
                        <div class="mb-3">
                            <label class="sr-only" for="inlineFormInputGroup">Username</label>
                            {{-- id="inlineFormInputGroup" --}}
                            <div class="calendar mb-2">
                                <input type="text" class="form-control" id="datetimepicker">
                                <i class="fas fa-calendar-alt text-muted icon"></i>
                            </div>
                            {{-- <div class="input-group-prepend">
                                <div class="input-group-text border-left-0">
                                    <i class="fas fa-calendar-alt text-muted"></i>
                                </div>
                            </div> --}}
                            {{-- <div class="input-group mb-2">
                                <input type="text" class="form-control border-right-0" id="inlineFormInputGroup">
                                <div class="input-group-prepend">
                                    <div class="input-group-text border-left-0">
                                        <i class="fas fa-calendar-alt text-muted"></i>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <label for="">Xe mấy chỗ</label>
                        <div class="mb-3">
                            <div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
                                <label class="btn btn-outline-light text-secondary border-right-0 active">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked> 4 chỗ
                                </label>
                                <label class="btn btn-outline-light text-secondary">
                                    <input type="radio" name="options" id="option2" autocomplete="off"> 7 chỗ
                                </label>
                                <label class="btn btn-outline-light text-secondary border-left-0">
                                    <input type="radio" name="options" id="option3" autocomplete="off"> 16 chỗ
                                </label>
                            </div>
                        </div>
                        <label for="">Loại xe</label>
                        <div class="mb-3">
                            <div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
                                <label class="btn btn-outline-light text-secondary active">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked> Tự động
                                </label>
                                <label class="btn btn-outline-light text-secondary">
                                    <input type="radio" name="options" id="option2" autocomplete="off"> Số sàn
                                </label>
                            </div>
                        </div>
                        <label for="">Hãng xe</label>
                        <div class="mb-3">
                            <select id="inputState" class="form-control">
                                <option selected>Toyota</option>
                                <option>Google</option>
                                <option>Yamaha</option>
                            </select>
                        </div>
                        <button class="btn btn-danger bg-gradient-danger w-100">
                            Tìm xe
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        jQuery('#datetimepicker').datetimepicker();
        jQuery.datetimepicker.setLocale('vi');
    </script>
@endpush