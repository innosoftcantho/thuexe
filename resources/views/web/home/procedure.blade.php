<div class="procedure py-4">
    <div class="container">
        <div class="d-flex justify-content-center py-4">
            <h1 class="font-weight-bold">
                Đặt xe như thế nào
            </h1>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-md-6 col-lg-4 text-center mb-4">
                <div class="image">
                    <img src="{{ asset('img/icon/1.png')}}">
                </div>
                <div class="my-3">
                    <h2 class="font-weight-bold">
                        Đặt xe
                    </h2>
                </div>
                <div>
                    Nhanh chóng đặt một chiếc xe ưng ý thông qua Website của chúng tôi
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center mb-4">
                <div class="image">
                    <img src="{{ asset('img/icon/2.png')}}">
                </div>
                <div class="my-3">
                    <h2 class="font-weight-bold">
                        Đặt xe
                    </h2>
                </div>
                <div>
                    Nhanh chóng đặt một chiếc xe ưng ý thông qua Website của chúng tôi
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center mb-4">
                <div class="image">
                    <img src="{{ asset('img/icon/3.png')}}">
                </div>
                <div class="my-3">
                    <h2 class="font-weight-bold">
                        Đặt xe
                    </h2>
                </div>
                <div>
                    Nhanh chóng đặt một chiếc xe ưng ý thông qua Website của chúng tôi
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center pb-3 w-100">
            <button class="btn btn-danger btn-more bg-gradient-danger">
                Xem chi tiết
            </button>
        </div>
    </div>
</div>