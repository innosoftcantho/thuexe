<div class="news bg-gray py-4">
    <div class="container">
        <div class="d-flex justify-content-center py-4">
            <h1 class="font-weight-bold">
                Tin tức
            </h1>
        </div>
        <div class="row pt-3">
            @for($i=0; $i<6; $i++)
            <div class="col-lg-6">
                <div class="card border shadow rounded mb-4">
                    <a href="#">
                        <div class="row">
                            <div class="col-5 pr-0">
                                <div class="image d-flex align-items-center justify-content-center h-100">
                                    <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg') }}" class="card-img-top">
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="card-body">
                                    <h2 class="card-title text-justify font-weight-bold">
                                        Lorem ipsum dolor sit amet, consetetur
                                    </h2>
                                    <div class="card-text d-none d-md-block text-justify mt-2">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, nsetetur sadipscing
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endfor
        </div>
        <div class="d-flex justify-content-center pt-2 pb-3 w-100">
            <button class="btn btn-danger btn-more bg-gradient-danger">
                Xem thêm
            </button>
        </div>
    </div>
</div>