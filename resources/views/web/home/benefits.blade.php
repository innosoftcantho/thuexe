<div class="benefit pt-4">
    <div class="container">
        <div class="d-flex justify-content-center pt-4">
            <h1 class="font-weight-bold">
                Lợi ích
            </h1>
        </div>
        <div class="row pb-5">
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/1.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Nhiều lựa chọn</h2>
                        <div>Hàng trăm loại xe đa dạng ở nhiều địa điểm trên cả nước, phù hợp với mọi mục đích của bạn</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/2.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Tin cậy</h2>
                        <div>Các xe đều có thời gian sử dụng dưới 3 năm và được bảo dưỡng thường xuyên</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/3.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Tin cậy</h2>
                        <div>Dễ dàng tìm kiếm, so sánh và đặt chiếc xe như ý với chỉ vài click chuột</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/4.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Hỗ trợ 24/7</h2>
                        <div>Có nhân viên hỗ trợ khách hàng trong suốt quá trình thuê xe</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/5.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Giá cả cạnh tranh</h2>
                        <div>Giá thuê được niêm yết công khai và rẻ hơn 10% so với giá truyền thống</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-4 col-lg-3">
                        <img src="{{ asset('img/benefit/6.png')}}" class="w-100" alt="">
                    </div>
                    <div class="col-8 col-lg-9">
                        <h2 class="font-weight-bold">Bảo hiểm</h2>
                        <div>An tâm với các gói bảo hiểm vật chất và tai nạn trong suốt quá trình thuê xe</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- @for($i=1; $i<7; $i++)
<div class="col-md-6 mt-5">
    <div class="row">
        <div class="col-4 col-lg-3">
            <img src="{{ asset('img/benefit/'.$i.'.png')}}" class="w-100" alt="">
        </div>
        <div class="col-8 col-lg-9">
            <h2 class="font-weight-bold">Nhiều lựa chọn</h2>
            <div>Hàng trăm loại xe đa dạng ở nhiều địa điểm trên cả nước, phù hợp với mọi mục đích của bạn</div>
        </div>
    </div>
</div>
@endfor --}}