<div class="highlight bg-gray py-4">
    <div class="container">
        <div class="d-flex justify-content-center py-4">
            <h1 class="font-weight-bold">
                Sản phẩm nổi bật
            </h1>
        </div>
        <div class="row pt-3">
            @for($i=0; $i<6; $i++)
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="bg-white rounded shadow p-4">
                    <h4 class="font-weight-bold">Kia Morning 1.25 S AT 2019</h4>
                    <div class="row">
                        <div class="col-6">
                            <i class="fas fa-couch mr-2"></i><small>4 chỗ</small>
                        </div>
                        <div class="col-6">
                            <i class="fas fa-gas-pump mr-2"></i><small>Xăng</small>
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('img/icon/pistons.svg') }}" class="mr-2" height="20"><small>125 cc</small>
                        </div>
                        <div class="col-6">
                            <i class="fas fa-sitemap mr-2"></i><small>Số tự động</small>
                        </div>
                    </div>
                    <div class="image d-flex justify-content-center align-items-center">
                        <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg') }}" class="w-100 py-2">
                    </div>
                    <button class="btn btn-danger bg-gradient-danger font-weight-bold w-100">
                        Chỉ từ 1.000.000đ/ngày
                    </button>
                </div>
            </div>
            @endfor
        </div>
        {{-- <div class="d-flex justify-content-center pt-2 pb-3 w-100">
            <button class="btn btn-danger btn-more bg-gradient-danger">
                Xem thêm
            </button>
        </div> --}}
    </div>
</div>