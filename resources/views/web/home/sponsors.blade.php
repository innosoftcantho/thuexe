<div class="sponsors pb-0 pt-5 pb-md-4">
    <div class="container">
        <div class="d-flex justify-content-center pb-4">
            <h1 class="font-weight-bold">
                Đối tác của chúng tôi
            </h1>
        </div>
        {{-- <div class="row pt-2">
            @for($i=0; $i<3; $i++)
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="d-flex justify-content-center rounded border shadow p-3">
                    <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg') }}" height="140" class="rounded mw-100">
                </div>
            </div>
            @endfor
        </div> --}}
        <div class="carousel slide modal-flick" data-type="multi" data-interval="3000" data-flickity='{
            "cellAlign": "center",
            "pageDots": false,
            "wrapAround": "true" ,
            "contain": true,
            "initialIndex": "0",
            "prevNextButtons": false,
            "draggable": ">2",
            {{-- "autoPlay": true, --}}
            "freeScroll": false,
            "friction": 0.8,
            "selectedAttraction": 0.2} '>
            @for($i=0; $i<4; $i++)
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="d-flex justify-content-center rounded border shadow p-3">
                    <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg') }}" height="140" class="rounded mw-100">
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>