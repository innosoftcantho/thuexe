@extends('layouts.web')
@section('content')

<!-- Search Section -->

@include('web.home.search')

<!-- Benefits Section -->

@include('web.home.benefits')

<!-- Products Section -->

@include('web.home.products')

<!-- Procedure Section -->

@include('web.home.procedure')

<!-- News Section -->

@include('web.home.news')

<!-- Sponsors Section -->

@include('web.home.sponsors')

@endsection
