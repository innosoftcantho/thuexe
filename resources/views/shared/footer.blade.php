@section('footer')

<div class="footer text-white bg-danger py-3">
    <div class="container">
        <div class="row pt-4">
            <div class="col-lg-5 mb-4 mb-lg-0">
                <ul class="list-unstyled">
                    <li class="mb-4">
                        <h3 class="text-uppercase font-weight-bold">liên hệ</h3>
                    </li>
                    <li class="mb-2">
                        <h5 class="font-weight-bold">Xe nhà</h5>
                    </li>
                    <li class="mb-2">
                        <h5 class="font-weight-normal">
                            45 Mạc Thiên Tích, Xuân Khánh, Ninh Kiều, Cần Thơ
                        </h5>
                    </li>
                    <li class="mb-2">
                        <h4>
                            <i class="fas fa-phone-alt mr-2"></i>0987654321 - 012344567
                        </h4>
                    </li>
                    <li>
                        <h4>
                            <i class="fas fa-envelope mr-2"></i>contact.carrental@gmail.com
                        </h4>
                    </li>
                    <li class="d-flex mt-4">
                        <a href="#">
                            <div class="icon rounded-circle bg-white text-secondary mr-2 p-1">
                                <i class="fab fa-facebook-f w-100"></i>
                            </div>
                        </a>
                        <a href="#">
                            <div class="icon rounded-circle bg-white text-secondary mr-2 p-1">
                                <i class="fab fa-youtube w-100"></i>
                            </div>
                        </a>
                        <a href="#">
                            <div class="icon rounded-circle bg-white text-secondary mr-2 p-1">
                                <i class="fab fa-instagram w-100"></i>
                            </div>
                        </a>
                        <a href="#">
                            <div class="icon rounded-circle bg-white text-secondary mr-2 p-1">
                                <i class="fab fa-twitter w-100"></i>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-3 mb-4 mb-lg-0">
                        <ul class="list-unstyled">
                            <li class="mb-4">
                                <h3 class="text-uppercase font-weight-bold">
                                    Giới thiệu
                                </h3>
                            </li>
                            <li>
                                <h4><a href="#">Về chúng tôi</a></h4>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5 mb-4 mb-lg-0">
                        <ul class="list-unstyled">
                            <li class="mb-4">
                                <h3 class="text-uppercase font-weight-bold">
                                    Chính sách
                                </h3>
                            </li>
                            <li>
                                <h4><a href="#">Chính sách bảo mật thông tin</a></h4>
                            </li>
                            <li>
                                <h4><a href="#">Quy chế hoạt động</a></h4>
                            </li>
                            <li>
                                <h4><a href="#">Sự cố khiếu nại</a></h4>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 mb-3 mb-lg-0">
                        <ul class="list-unstyled">
                            <li class="mb-4">
                                <h3 class="text-uppercase font-weight-bold">
                                    Hỗ trợ
                                </h3>
                            </li>
                            <li>
                                <h4><a href="#">Hướng dẫn thuê xe</a></h4>
                            </li>
                            <li>
                                <h4><a href="#">Hợp đồng thuê xe tự lái</a></h4>
                            </li>
                            <li>
                                <h4><a href="#">Cẩm nang thuê xe tự lái</a></h4>
                            </li>
                            <li>
                                <h4><a href="#">Câu hỏi thường gặp</a></h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h5 class="copyright bg-danger py-2 mb-0" align="center">
        Bản quyền © 2018 thuộc về Xe nhà.
    </h5>
</div>