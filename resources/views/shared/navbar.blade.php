<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top shadow py-lg-0 px-0 px-md-3">
    <div class="container">
        <button class="btn btn-danger bg-gradient-danger rounded">
            Trở thành đối tác
        </button>
        <a class="navbar-brand text-danger py-0 mx-auto ml-lg-auto pr-lg-3 mr-lg-5" href="#" id="logoNavScroll">
            <h1 class="font-weight-bold mb-0">Xe Nhà</h1>
        </a>
        <button class="navbar-toggler mr-lg-3 border-0 px-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse my-2 my-lg-0 ml-lg-5" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link py-lg-3" href="#">Trang Chủ<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-lg-3" href="#">Giới thiệu</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle py-lg-3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Thuê xe
                    </a>
                    <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Lamborgini</a>
                        <a class="dropdown-item" href="#">Ferrari</a>
                        <a class="dropdown-item" href="#">Ford</a>
                    </div>
                </li>
                <li class="nav-item ml-0 ml-lg-1 mr-2">
                    <a class="nav-link py-lg-3 pr-0" href="#">Đăng nhập</a>
                </li>
            </ul>
        </div>
    </div>
</nav>